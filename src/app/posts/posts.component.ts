import { Component, OnInit } from '@angular/core';
import {PostsService} from './posts.service'; // import posts service

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

posts;
currentPost;
isloading:Boolean = true;

select(post){
this.currentPost = post;  // li מגיע מ post
}

addPost(post){ // Add post function that declared at posts.component.html
  //this.posts.push(user) // only local not from firebase
    this._postsService.addPost(post); // addPost function is at posts.service
}

deletePost(post){
   // this.posts.splice(
    //this.posts.indexOf(post),1  ) // indexOf(post) is the location and 1 is the quantity
    this._postsService.deletePost(post);
    
}

updatePost(post){ // This function will send the data to posts.service 
  this._postsService.updatePost(post); 
}



constructor(private _postsService: PostsService) { //  Declaring an object PostsService form type PostsService to allow as to make changes in 'ngOnInit'.
                                                  
  }

  ngOnInit() { // What we want to load 

    this._postsService.getPosts().subscribe(postsData => {this.posts = postsData; this.isloading = false; console.log(this.posts)}); // We don't need ; at the last command
    /*
      subscribe: to pull the data that we get from observable, inside there is arrow notation
      זה כמו :
      function (postsData) {
        this.posts = postsData;
      }   

      isloading is the end of spinner
    */
  }

}

import { Injectable } from '@angular/core';
import {Http} from '@angular/http'; // import for http pulling data from server 
import {AngularFire} from 'angularfire2'; // import for firebase
import 'rxjs/add/operator/map'; // import for map function - that can change formats
import 'rxjs/add/operator/delay'; 

@Injectable()
export class PostsService {

// private _url = 'http://jsonplaceholder.typicode.com/posts'; //we remove this line because we want to read from firebase

//constructor(private _http:Http) { } // Declering on a private attribute from type Http that was imported
constructor(private _af:AngularFire) { } // Declering on provate attribute af=AngularFire

postsObservable;

getPosts(){
this.postsObservable = this._af.database.list('/posts').map(
  posts => {
    posts.map(
      post => {
        post.userName = [];
        for(var u in post.users){
          post.userName.push(
            this._af.database.object('/users/' + u)
          )
        }
      }
    );
    return posts;
  }
) // if we choose object (insted of list) the syntax will be:  object('/users/1')    ('users' is the name of table in firebase)
  return this.postsObservable;

 // return this._http.get(this._url).map(res => res.json()).delay(2000)     // res is just the name, like result
  /* 
 שהצהרנו עליה בבנאי  http בשורה למעלה, השתמשנו בתכונה 
 url כדי לשלוף את המידע מתוך ה  observable שמחזירה  get אחר כך עשינו 
 קיבלו מחרוזת observable מתוך ה  
 מאפשרת לי להמיר את המחרוזת לפורמט כלשהו map  הפונקציה 
 json מאפשרת לי להמיר לפורמט json הפונקציה  

  */
	}

addPost(post){ // We call to this function from posts.component.ts
  this.postsObservable.push(post);   
}


updatePost(post){
  let postKey = post.$key; // postKey is uniqe =>  like id
  let postData = {title:post.title, body:post.body};
  this._af.database.object('/posts/' + postKey).update(postData); // update in firebase database
}

deletePost(post){
  let postKey = post.$key;
  this._af.database.object('/posts/' + postKey).remove();
}



}

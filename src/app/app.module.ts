import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { PostsComponent } from './posts/posts.component';
import {PostsService} from './posts/posts.service';
import { PostComponent } from './post/post.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { UserFormComponent } from './user-form/user-form.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component'; // import for service 
import { AngularFireModule } from 'angularfire2'; // for firebase 

var config = { // sdk arry for firebase
    apiKey: "AIzaSyA0ebV--7jrykJJytAZf40ObD-Y1-4-Zkg",
    authDomain: "angular-home-8b14e.firebaseapp.com",
    databaseURL: "https://angular-home-8b14e.firebaseio.com",
    storageBucket: "angular-home-8b14e.appspot.com",
    messagingSenderId: "821746438469"
  };

const appRoutes: Routes = [
  { path: 'users', component: UsersComponent },
  { path: 'posts', component: PostsComponent },
  { path: '', component: PostsComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    PostsComponent,
    PostComponent,
    SpinnerComponent,
    UserFormComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,    
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(config) // for firebase. config is the name of array
  ],
  providers: [PostsService], //We created new service and we need to import it and than to add it to providers
  bootstrap: [AppComponent]
})
export class AppModule { }

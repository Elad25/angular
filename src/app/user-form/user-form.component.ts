import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Post } from '../post/post';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Output() postAddEvent = new EventEmitter<Post>();
  post:Post= {// userId:'', id:'', 
              title:'', body:''}; // We must declare all variable that in the class
  constructor() { }


  onSubmit(form:NgForm){ // The form will add the values with the addPost function that implement at posts.component.html
    console.log(form);
    this.postAddEvent.emit(this.post); // The metod sends the new post to the parent (posts) by emit function 
    this.post={ // This array will clear the input fields after send
      //userId:'',
    //  id:'',
      title:'',
      body:''
    }
  }
  ngOnInit() {
  }

}
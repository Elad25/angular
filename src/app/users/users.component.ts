import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users table { cursor: default; }
    .users teable:hover { background: red; } 
  `]
})
export class UsersComponent implements OnInit {

  users = [
    {name:'John',email:'john@gmail.com'},
    {name:'Jack',email:'jack@gmail.com'},
    {name:'Alice',email:'alice@yahoo.com'}
  ]
  
  constructor() { }

  ngOnInit() {
  }

}
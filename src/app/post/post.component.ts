import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Post} from './post';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css'],
  inputs:['post']
})
export class PostComponent implements OnInit {

post:Post; // השמה, פוסט הוא מסוג פוסט  

@Output() deleteEvent = new EventEmitter<Post>();
@Output() editEvent = new EventEmitter<Post>(); // EventEmitter passing the data from son to father at posts.component.html

isEdit:Boolean = false;
editButtonText = "Edit";

sendDelete(){
  this.deleteEvent.emit(this.post);
}

toggleEdit(){
  this.isEdit = !this.isEdit;
  this.isEdit ? this.editButtonText = 'Save' : this.editButtonText = 'Edit';
  if(!this.isEdit){
    this.editEvent.emit(this.post); // emit will send the data to posts.component.html
  }
}

// The left post is the attribute
// The right Post is the class name - Post is object of attributes
  constructor() { }


  ngOnInit() {
  }

}
